# Link Builder

## Description

A simple tool for building URLs to test the configuration of

1. Force http to https
2. Force www to non-www or vice versa

Get rid of typing the URLs to address bar of your browser! :)

## GitLab Pages

After GitLab CI/CD pipeline job finishes, visit [this URL](https://kburakozdemir.gitlab.io/httpsnonwww/).

## Versions

| Software | Version  |
|----------|----------|
| node     | 10.16.0  |
| npm      | 6.9.0    |

Please take a look at `package.json` file for other software used (and their documentation if needed).

## Usage

Run `npm ci` after cloning repo. (If `npm ci` does not work, use `npm install`.)

### `npm` Commands

`watch` -> `npm run gulp`

`build` -> `npm run build`

After build, you can deploy the created `public` folder.

### Edit

Only edit files in `app` folder.
