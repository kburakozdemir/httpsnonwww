// Ref: https://www.sitepoint.com/jquery-document-ready-plain-javascript/

var callback = function () {
  // Handler when the DOM is fully loaded
  console.log("DOM is loaded");
};

if (
  document.readyState === "complete" ||
  (document.readyState !== "loading" && !document.documentElement.doScroll)
) {
  callback();
} else {
  document.addEventListener("DOMContentLoaded", callback);
}

// Coming from original project

var mytext;

function btnyapaction(myurltext) {
  const d = new Date();
  const n = d.getTime();
  jQuery("#mylinks").hide().html("");
  jQuery("#mylinks").html(myurltext);
  mytext =
    '<a href="http://www.' +
    myurltext +
    "?_=" +
    n +
    '" target="_blank">http://www.' +
    myurltext +
    '</a><br> \
  <a href="http://' +
    myurltext +
    "?_=" +
    n +
    '" target="_blank">http://' +
    myurltext +
    '</a><br> \
  <a href="https://www.' +
    myurltext +
    "?_=" +
    n +
    '" target="_blank">https://www.' +
    myurltext +
    '</a><br> \
  <a href="https://' +
    myurltext +
    "?_=" +
    n +
    '" target="_blank">https://' +
    myurltext +
    "</a>";
  jQuery("#mylinks")
    .append("<br/><br/>" + mytext)
    .show("slow");
}

jQuery(document).ready(function () {
  jQuery("#btnyap").click(function () {
    const myurl = jQuery("#myurl").val();
    if (myurl) {
      btnyapaction(myurl);
    } else {
      alert("domain girmelisiniz");
    }
  });

  btnyapaction("sayfaevi.com");
});
