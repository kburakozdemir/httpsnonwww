var gulp = require("gulp"),
  clean = require("gulp-clean"),
  panini = require("panini"),
  browserSync = require("browser-sync").create(),
  // folders
  reload = browserSync.reload,
  folder = {
    src: "app/",
    build: "public/",
  };

gulp.task("cleandist", function () {
  return gulp.src("public", { read: false }).pipe(clean());
});

// In order to add folders from node_modules
// uncomment gulp.src array entries or add folders there.
gulp.task("copyvendor", ["cleandist"], function () {
  return gulp
    .src(["node_modules/@fortawesome/**/*"], {
      base: "./node_modules/",
    })
    .pipe(gulp.dest("public/vendor"));
});

gulp.task("copyjs", ["cleandist"], function () {
  return gulp
    .src(["app/js/**/*"], {
      base: "./app/",
    })
    .pipe(gulp.dest("public"));
});

gulp.task("copycss", ["cleandist"], function () {
  return gulp
    .src(["app/css/**/*"], {
      base: "./app/",
    })
    .pipe(gulp.dest("public"));
});

gulp.task("copyassets", ["cleandist"], function () {
  return gulp
    .src(["app/assets/**/*"], {
      base: "./app/",
    })
    .pipe(gulp.dest("public"));
});

gulp.task("html", ["cleandist"], function () {
  var url = "html/",
    htmlRoot = folder.src + url,
    out = folder.build;
  return gulp
    .src(htmlRoot + "pages/" + "**/*.html")
    .pipe(
      panini({
        root: htmlRoot + "pages/",
        layouts: htmlRoot + "layouts/",
        partials: htmlRoot + "partials/",
        helpers: htmlRoot + "helpers/",
        data: htmlRoot + "data/",
      })
    )
    .pipe(gulp.dest(out));
});

gulp.task("resetPages", (done) => {
  panini.refresh();
  done();
});

gulp.task(
  "serve:dist",
  ["cleandist", "copyvendor", "copyjs", "copycss", "copyassets", "html"],
  function () {
    browserSync.init({
      server: "./public",
      port: 3000,
    });

    gulp.watch(
      [
        folder.src + "html/{pages,layouts,partials,helpers,data}/**/*.html",
        folder.src + "{js,css}/**/*.*",
      ],
      [
        "resetPages",
        "cleandist",
        "copyvendor",
        "copyjs",
        "copycss",
        "copyassets",
        "html",
        reload,
      ]
    );
  }
);

gulp.task("build", [
  "resetPages",
  "cleandist",
  "copyvendor",
  "copyjs",
  "copycss",
  "copyassets",
  "html",
]);
